var Excel = require('exceljs');
require('fs');
var mkdirp = require('mkdirp');
const Brkill = require('browser-kill');
var wb = new Excel.Workbook();
const pptr = require('puppeteer-core');
var propertiesReader = require('properties-reader');
var properties = propertiesReader(__dirname + '/Files/config.file');
let date_ob = new Date();
let dt = date_ob.getDate() + '-' + (date_ob.getMonth() + 1) + '-' + date_ob.getFullYear() + '-' + date_ob.toLocaleTimeString();
const screenshotsPath = __dirname + '/Screenshots/' + dt;
mkdirp(screenshotsPath, function (err) {
    if (err) console.error(err)
    else console.log('Folder for screenshots created... !')
});
(async () => {

    wb.xlsx.readFile(__dirname + '/Files/users-dev.xlsx').then(async function () {
        try {
            var sh = wb.getWorksheet("users-dev");
            console.log(sh.rowCount);
            for (let i = 1; i <= sh.rowCount; i++) {
                console.log('Mail Id:  ' + sh.getRow(i).getCell(2).value.toString());
                let mailId = sh.getRow(i).getCell(2).value.toString();
                console.log('Username:  ' + sh.getRow(i).getCell(4).value.toString());
                let userName = sh.getRow(i).getCell(4).value.toString();
                console.log('Vip Fan:  ' + sh.getRow(i).getCell(5).value.toString());
                let url = sh.getRow(i).getCell(6).value.toString();
                console.log('URL:  ' + sh.getRow(i).getCell(6).value.toString());
                let browserPath = properties.get('main.Browser').toString();
                const browser = await pptr.launch({

                    executablePath: browserPath,
                    // product: 'chrome',
                    pipe: true,
                    args: ['--start-fullscreen',
                        '--disable-dev-shm-usage',
                        '--agc-startup-min-volume=22',
                        '--disable-volume-adjust-sound',
                        '--allow-file-access-from-files',
                        '--use-fake-ui-for-media-stream',
                        '--allow-file-access',
                        '--use-file-for-fake-audio-capture=Audio.wav',
                        '--use-file-for-fake-video-capture=out.y4m',
                        '--use-fake-device-for-media-stream',
                    ],
                    ignoreDefaultArgs: ['--mute-audio'],
                    defaultViewport: null,
                    headless: true,
                    timeout: 0,
                    // cacheDisabled: true,
                });
                const [page] = await browser.pages();
                console.log(i + ' : Navigating to landing page...!');
                await page.setDefaultNavigationTimeout(0);
                await page.goto(url, { waitUntil: "load", timeout: 0 });
                await page.waitFor(3000);
                const context = browser.defaultBrowserContext();
                context.overridePermissions(url, ['camera', 'microphone', 'midi', 'midi-sysex', 'notifications']);
                await page.screenshot({ "path": screenshotsPath + "/Landingpage_" + i + ".jpeg" });

                await page.waitForXPath("//input[@formcontrolname='email']", { timeout: 0 });
                await page.$x("//input[@formcontrolname='email']").then(async ele => {
                    await ele[0].click();
                });
                await page.waitFor(1000)
                await page.$x("//input[@formcontrolname='email']").then(async ele => {
                    await ele[0].type(mailId);
                });
                await page.waitFor(2000);
                await page.waitForXPath('//button[contains(text(),"Verify Email")]', { timeout: 0 });
                await page.$x('//button[contains(text(),"Verify Email")]').then(async ele => {
                    await ele[0].click();
                });
                await page.waitFor(2000);
                await page.waitForXPath("//input[@formcontrolname='name']", { timeout: 0 });
                await page.$x("//input[@formcontrolname='name']").then(async ele => {
                    await ele[0].type(userName);
                });
                await page.waitFor(2000);
                await page.waitForXPath('//button[contains(text(),"Update Alias")]', { timeout: 0 });
                await page.$x('//button[contains(text(),"Update Alias")]').then(async ele => {
                    await ele[0].click();
                });
                await page.waitFor(7000);
                await page.waitForXPath("//div[@class='control-wrapper']//button[2]", { timeout: 0 });
                await page.$x("//div[@class='control-wrapper']//button[2]").then(async ele => {
                    await ele[0].click();
                });
                await page.waitFor(100 * i);
                await page.screenshot({ "path": screenshotsPath + "/Broadcasting_" + i + ".jpeg" });
            }
            await page.waitFor(1000);
            await Brkill.chrome();
        }
        catch (e) {
            await Brkill.chrome();
            console.log("Killing the process......" + '\n' + "The exeception occured is :" + e);
        }
    });


})();
